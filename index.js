/**
 * @format
 */
import 'react-native-gesture-handler';

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import SaveViewRender from './SaveViewRender';
import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStore } from 'redux'
import { Provider} from 'react-redux'
import noteReducers from './reducers/reducers.js'

const store = createStore(noteReducers)

const Main = () => {
    console.log('Main called.');
    return(
        <NavigationContainer>
            <Provider store = {store}>
                <App />
            </Provider>
        </NavigationContainer>
    )
}


    /*
    return(
        <Provider store={store}>
            <App />
        </Provider>
    )
}
*/
export default Main

AppRegistry.registerComponent(appName, () => Main);
