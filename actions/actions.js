import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';

//Unique id:s
// TODO: read currentId from store to stop inconsistencies between store state and id:s
// Sometimes store resets while the id:s do not.

let nextId = 0

const saveNote = (name, text) => {
	return{
		type: 'SAVE_NOTE',
		payload: {
			name: name,
			text: text,
			id: nextId++
		}
	};
};

const deleteNote = (id) => {
	return{
		type: 'DELETE_NOTE',
		payload: {
            id: id
		}
	};
};

const toggleView = (view) => {
	return{
		type: 'TOGGLE_VIEW',
		payload: {
            view: true
		}
	};
};

const selectId = (id) => {
    return{
        type: 'SELECT_ID',
        payload: {
            id: id
        }
    };
};

export {saveNote, toggleView, selectId, deleteNote}
