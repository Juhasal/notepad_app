import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
  Provider,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';

import {saveNote, toggleView, selectId, deleteNote} from '../actions/actions.js'
//REDUCERS

export const dataHandler = (noteList = [], action) => {
	switch(action.type){
		case('SAVE_NOTE'):
			return [...noteList, action.payload];
		case('DELETE_NOTE'):
		    return noteList.filter(item =>  (item.id !== action.payload.id));
		default:
		    return noteList;
	};
};

export const numberHandler = (currentId = 0, action) => {
	switch(action.type){
		case('SELECT_ID'):
			return currentId = action.payload;
		default:
		    return currentId;
	};
};

export const viewHandler = (currentView = true, action) => {
	switch(action.type){
		case('TOGGLE_VIEW'):
			return !currentView;
		default:
		    return currentView;
	};
};


const noteReducers = combineReducers({
  	dataHandler: dataHandler,
  	numberHandler: numberHandler,
  	viewHandler: viewHandler
})


export default noteReducers