import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

//Colour themes
/*
const color_dark = '#2d767f';
const color_mid = '#b4f1f1';
const color_light = '#ecfffb';
*/
export const color_dark = '#1c7293';
export const color_mid = '#b9e3c6';
export const color_light = 'white';
//'#f1f2eb';
export const color_text = '#272932';

const styles = StyleSheet.create( {
    modal:{
        color: color_light,
        backgroundColor: color_light,
        padding: 30,
        alignItems: "center",
        borderRadius: 5,
        margin: 20,
        elevation: 5
    },
    button:{
        padding: 5,
        margin: 5,
        borderRadius: 2,
        backgroundColor: color_mid,
        color: color_dark,
    },
    text:{
        fontSize: 15,
        color: color_text,
        alignSelf:'center'
    },
    notebox:{
        textAlignVertical: "top",
        borderColor:color_dark,
        margin: 10,
        borderRadius: 3,
        height: 160,
        width: 'auto',
        borderWidth: 1,
    },
    box2:{
        textAlignVertical: "top",
        margin: 10,
        borderRadius: 3,
        height: 40,
        width: '100%',
        borderColor:color_dark,
        borderWidth: 1,
    },
    header:{
        flex: 1,
        backgroundColor: color_mid,
        justifyContent: 'center'
    },
    flexedView:{
        flex: 7,
        flexDirection: 'column',
        backgroundColor: color_mid,
        justifyContent: 'center'
    },
    flexedTextBoxContainer:{
        flex: 8,
        justifyContent:'flex-start',
        flexDirection:'column',
        alignSelf :'stretch',
        backgroundColor:color_light,

    },
    flexedButton:{
        backgroundColor:color_dark,
        padding: 5,
        borderRadius: 3,
        flex:1,
        flexDirection: 'row',
        alignSelf:'center',
        justifyContent:'space-around',
        margin:10
    },
    flexedButtonView:{
        flex: 1,
        justifyContent:'space-around',
        flexDirection:'row',
        backgroundColor:color_mid,
        alignSelf :'center',
        alignContent:'space-around',
        alignItems:'center',
        padding:10
    },
    flexedTextInput:{
        flex:1,
        textAlignVertical: "top",
        borderColor: color_dark,
        margin: 10,
        borderRadius: 3,
        height: 160,
        width: 'auto',
        borderWidth: 1,
        color:color_text,
    },
    flexedContainer:{
        flex: 1,
        justifyContent:'center',
        flexDirection:'column',
        alignSelf :'stretch',
        backgroundColor: color_dark,
    },
    colors:{
        color: color_light,
        backgroundColor: color_mid,
    },
    text:{
        fontFamily: 'sans-serif-light',
        fontSize: 18,
    }

});

export default styles;