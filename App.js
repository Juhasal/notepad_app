import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
  ImageBackground
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from './SaveViewRender.js';
import WriteView from './Components/WriteView.js';
import LoadView from './Components/LoadView.js';
import FlexTest from './Components/Flextest.js';
import UITests from './Components/UITests.js';
import FormView from './Components/FormView.js'

import styles from './styles/styles.js';
import {color_light, color_dark, color_mid, color_text} from './styles/styles.js';
import {saveNote, toggleView} from './actions/actions.js';
import noteReducers from './reducers/reducers.js';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const backgroundImage = require('./icons/bg_test.png');

export const Stack = createStackNavigator();

const SaveModal = (text) => {
    console.log(text + saveName);
};

const HomeScreen = ({navigation}) => {
  return(
    <ImageBackground source={backgroundImage} style={{    width:'100%',
                                                          height:'100%',
                                                          resizeMode: "cover",
                                                          justifyContent: "center"}}>

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin:20}}>

          <TouchableHighlight
          onPress={() => navigation.push('Write')}
          activeOpacity={0.6}
          underlayColor={color_dark}
          style={{...styles.button, backgroundColor:color_dark, color:color_dark, margin:20}}>
              <Text style={{...styles.text, color:'white'}}> New Note </Text>
          </TouchableHighlight>

            <TouchableHighlight
            onPress={() => navigation.push('Load')}
            activeOpacity={0.6}
            underlayColor={color_dark}
            style={{...styles.button, backgroundColor:color_dark, color:color_dark, margin:20}}>
                <Text style={{...styles.text,color:'white'}}> Load Note </Text>
            </TouchableHighlight>

            <TouchableHighlight
            onPress={() => navigation.push('Forms')}
            activeOpacity={0.6}
            underlayColor={color_dark}
            style={{...styles.button, backgroundColor:color_dark, color:color_dark, margin:20}}>
                <Text style={{...styles.text,color:'white'}}> Forms </Text>
            </TouchableHighlight>


        </View>
    </ImageBackground>

  );
};

//Functions to call the views
//put navigation as a prop.

function WriteScreen( {navigation} ) {
    return (
        <View>
            <WriteView navigation={ navigation } />
        </View>
    );
}

function LoadScreen({navigation}) {
    return (
        <View>
            <LoadView navigation={navigation}/>
        </View>
    );
}

function FormScreen({navigation}) {
    return (
        <View>
            <FormView navigation={navigation}/>
        </View>
    );
}

const App = () => {
    const dispatch = useDispatch();
    const notesState = useSelector(state => state.dataHandler);
    const viewState = useSelector(state => state.viewHandler);

    const [text, setText] = React.useState('');
    const [saveViewToggle, setSaveViewToggle] = React.useState(false);
    const toggleState = (saveViewToggle) => setSaveViewToggle(!saveViewToggle);
    const [viewSelector, setViewSelector] = React.useState(false);
/*
    return(
        <View>
            <UITests />
        </View>
    );
*/

    return(
      <Stack.Navigator initialRouteName="Home">

        <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
                title: 'Home',
                headerStyle:{backgroundColor:color_dark},
                headerTitleStyle:{fontFamily:'sans-serif', color:'white'}
                }} />

        <Stack.Screen name="Write" component={WriteScreen}
            options={{
               title: 'Notes',
               headerStyle:{backgroundColor:color_dark},
               headerTitleStyle:{fontFamily:'sans-serif',color:'white'}
               }} />

        <Stack.Screen name="Load" component={LoadScreen}
            options={{
               title: 'Load',
               headerStyle:{backgroundColor:color_dark},
               headerTitleStyle:{fontFamily:'sans-serif',color:'white'}
               }} />

        <Stack.Screen name="Forms" component={FormScreen}
           options={{
              title: 'Forms',
              headerStyle:{backgroundColor:color_dark},
              headerTitleStyle:{fontFamily:'sans-serif',color:'white'}
              }} />


      </Stack.Navigator>
    );
}

export default App
