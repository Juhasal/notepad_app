import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from '../SaveViewRender.js';
import styles from '../styles/styles.js';
import {saveNote, toggleView} from '../actions/actions.js'
import noteReducers from '../reducers/reducers.js'


const FlexTest = () => {
    return(
        <View style={{backgroundColor:'#43dde6', width: '100%', height: '100%'}}>
            <View style={styles.flexedContainer}>
                <Text style={{alignSelf:'center', color:'white'}}>
                    NOTES
                </Text>
            </View>
            <View style={styles.flexedTextBoxContainer}>
                <TextInput
                    onChangeText={() =>{}}
                    defaultValue={'Write here'}
                    multiline={true}
                    numberOfLines={8}
                    style={styles.flexedTextInput}>
                </TextInput>
            </View>
            <View style={styles.flexedButtonView}>
                <TouchableHighlight
                  activeOpacity={0.6}
                  underlayColor="#DDDDDD"
                  style={styles.flexedButton}>
                    <Text style={{color:'white'}}> SAVE </Text>
                </TouchableHighlight>
                <TouchableHighlight
                  activeOpacity={0.6}
                  underlayColor="#DDDDDD"
                  style={styles.flexedButton}>
                    <Text style={{color:'white'}}> LOAD </Text>
                </TouchableHighlight>
            </View>
        </View>
    );
}

export default FlexTest;