import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from '../SaveViewRender.js';
import styles from '../styles/styles.js';
import {saveNote, toggleView, selectId, deleteNote} from '../actions/actions.js'
import noteReducers from '../reducers/reducers.js'
import { useNavigation } from '@react-navigation/native';

const FormView = ( {navigation} ) => {
    return(
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', margin:20}}>
            <Text>
                Forms
            </Text>
        </View>
    )
}

export default FormView;