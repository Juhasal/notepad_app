import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from '../SaveViewRender.js';
import styles from '../styles/styles.js';
import {saveNote, toggleView, selectId, deleteNote} from '../actions/actions.js'
import noteReducers from '../reducers/reducers.js'
import { useNavigation } from '@react-navigation/native';

//import {Stack} from '../App.js'


const WriteView = ( {navigation} ) => {
    const dispatch = useDispatch();

    //store states
    const notesState = useSelector(state => state.dataHandler);
    const viewState = useSelector(state => state.viewHandler);
    const idState = useSelector(state => state.numberHandler);

    //local states
    const [text, setText] = React.useState('');
    const [defaultText, setDefaultText] = React.useState('');
    const [saveViewToggle, setSaveViewToggle] = React.useState(true);
    const toggleState = (saveViewToggle) => setSaveViewToggle(!saveViewToggle);
/*
    if (typeof(notesState[idState.id].text) == 'undefined'){
        console.log('test_undefined');
        dispatch(selectId(0));
    }
*/

//This thing is throwing errors: notesState[idState.id].text is undefined
    if (idState != 0 && defaultText != notesState[idState.id].text)  {
        console.log('testing text input saving');
        console.log(idState.id);
        console.log(notesState[idState.id]);
        console.log(notesState[idState.id].text);
        setDefaultText(notesState[idState.id].text);
        console.log('text:' + defaultText);
    }

//Header:
/*
<View style={styles.flexedContainer}>
    <Text style={{alignSelf:'center', color:'white'}}>
        NOTES
    </Text>
</View>
*/
    return (

        <View style={{backgroundColor:'#43dde6', width: '100%', height: '100%'}}>

            <View style={styles.flexedTextBoxContainer}>
                <TextInput
                    onChangeText={text => setText(text)}
                    defaultValue={defaultText}
                    multiline={true}
                    numberOfLines={8}
                    style={{...styles.flexedTextInput,...styles.text,fontSize:16}}
                />
            </View>

            <View style={styles.flexedButtonView}>

                <TouchableHighlight
                onPress={() => setSaveViewToggle(!saveViewToggle)}
                activeOpacity={0.6}
                underlayColor="#DDDDDD"
                style={styles.flexedButton}>
                    <Text style={{...styles.text,color:'white', fontSize:16}}> SAVE </Text>
                </TouchableHighlight>

                <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.button.color}
                style={styles.flexedButton}
                onPress={() => {dispatch(toggleView(true)); console.log(viewState); navigation.push('Load')}}>
                    <Text style={{...styles.text,color:'white',fontSize:16}}> LOAD </Text>
                </TouchableHighlight>

            </View>
            <SaveViewRender saveViewToggle={saveViewToggle} text={text} toggleState={toggleState} />
        </View>
    );
}

export default WriteView;