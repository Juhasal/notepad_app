import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
  Image,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from '../SaveViewRender.js';
import styles from '../styles/styles.js';
import {color_light, color_dark, color_mid, color_text} from '../styles/styles.js';
import {saveNote, toggleView, selectId, deleteNote} from '../actions/actions.js'
import noteReducers from '../reducers/reducers.js'

const iconShare = require('../icons/share.png');

const UITests = () => {
    return(
        <ScrollView style={{height:'100%',width:'100%',backgroundColor:color_light}}>
            <View style={{
                    backgroundColor:color_dark,
                    height:40,
                    flexDirection:'row',
            }}>
                <Image
                style={{height:'100%',
                        resizeMode:'contain',
                        alignSelf:'center',
                        flex:1,
                        }}
                source={require('../icons/menu-1.png')}
                />
                <Text style={{color:'white',textAlign:'center',alignSelf:'center',flex:6}}>
                    UI TESTS
                </Text>
            </View>

            <View elevation={3} style={{
                padding:10,
                margin:10,
                backgroundColor:'white',
                borderRadius:10,
                height:100,
                shadowRadius: 5,
                shadowOpacity: 1,
                shadowOffset: {height:2, width:2},
                shadowColor: "black",
                flexDirection:'row',
                alignSelf :'auto',
                alignContent:'auto',
                }} >
                <Image
                style={{ height:'100%', width:'auto',flex:1,resizeMode:'contain'}}
                source={iconShare}
                />
                <Image
                style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                source={iconShare}
                />
                <Image
                style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                source={iconShare}
                />

            </View>

            <View elevation={3} style={{
                            padding:10,
                            margin:10,
                            backgroundColor:'white',
                            borderRadius:10,
                            height:100,
                            shadowRadius: 5,
                            shadowOpacity: 1,
                            shadowOffset: {height:2, width:2},
                            shadowColor: "black",
                            flexDirection:'row',
                            alignSelf :'auto',
                            alignContent:'auto',
                            }} >
                            <Image
                            style={{ height:'100%', width:'auto',flex:1,resizeMode:'contain'}}
                            source={require('../icons/user-4.png')}
                            />
                            <Image
                            style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                            source={require('../icons/user-4.png')}
                            />
                            <Image
                            style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                            source={require('../icons/user-4.png')}
                            />

                        </View>

<View elevation={3} style={{
                padding:10,
                margin:10,
                backgroundColor:'white',
                borderRadius:10,
                height:100,
                shadowRadius: 5,
                shadowOpacity: 1,
                shadowOffset: {height:2, width:2},
                shadowColor: "black",
                flexDirection:'row',
                alignSelf :'auto',
                alignContent:'auto',
                }} >
                <Image
                style={{ height:'100%', width:'auto',flex:1,resizeMode:'contain'}}
                source={iconShare}
                />
                <Image
                style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                source={iconShare}
                />
                <Image
                style={{ height:'100%', width:40,flex:1,resizeMode:'contain'}}
                source={iconShare}
                />

            </View>


        </ScrollView>


    );
}

export default UITests;