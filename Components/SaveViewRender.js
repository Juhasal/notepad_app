import React from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';

import logger from 'redux-logger'



const SaveViewRender = (props) => {
    const saveViewToggle = props.saveViewToggle;
    const text = props.text;
    console.log(saveViewToggle);
    console.log(text);

    const [noteName,setNoteName] = React.useState('');

    return(
        <View>
            <Modal
                style={styles.modal}
                animationType="slide"
                transparent={true}
                visible={saveViewToggle}
                onRequestClose={() => {
                  Alert.alert("Modal has been closed.");
                  }}
            >
                <View style={styles.modal}>
                    <Text>
                        Give a name to your note
                    </Text>
                    <TextInput
                        style={styles.box2}

                        onChangeText={noteName => setNoteName(noteName)}
                        defaultValue={''}
                    />
                    <Button
                        color='#43dde6'
                        title='Save note'
                        onPress={() => {store.dispatch(saveNote(noteName,text)); console.log('setFalse'); props.toggleState(saveViewToggle);}}
                    />
                </View>
            </Modal>
        </View>
    );
};

export default SaveViewRender;