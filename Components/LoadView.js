import React, {Component} from 'react';
import {
  SafeAreaView,
  TextInput,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Button,
  Alert,
  Modal,
  TouchableHighlight,
  FlatList,
} from 'react-native';

import {createStore, combineReducers, applyMiddleware} from 'redux';
import {useDispatch, useSelector} from 'react-redux';
import logger from 'redux-logger'

import SaveViewRender from '../SaveViewRender.js';
import styles from '../styles/styles.js';
import {color_light, color_dark, color_mid, color_text} from '../styles/styles.js';
import {saveNote, toggleView, selectId, deleteNote} from '../actions/actions.js'
import noteReducers from '../reducers/reducers.js'

//const selected = 2;
//const idState = useSelector(state => state.numberHandler);

const Item = ({title, selected}) => {
    const dispatch = useDispatch();
    return(
        <View>
            <TouchableHighlight
              activeOpacity={0.6}
              underlayColor={color_mid}
              style={{...styles.flexedButton, width: '90%', margin:1, backgroundColor:color_mid,
              backgroundColor: title.id == selected.id ? color_dark : color_mid
              }}
              onPress={() => {console.log(title.id); console.log(selected); dispatch(selectId(title.id)) }}
              >
                <Text style={{...styles.text, color:color_dark,
                color: title.id == selected.id ? color_light : color_dark
                }}>
                    {title.name}
                </Text>
            </TouchableHighlight>
        </View>
    )
}

const LoadView = ({navigation}) => {
    const dispatch = useDispatch();
    const notesState = useSelector(state => state.dataHandler);
    const viewState = useSelector(state => state.viewHandler);
    const idState = useSelector(state => state.numberHandler);
    const [text, setText] = React.useState('');
    //const [selectedId, setSelectedId] = React.useState(0);
    const [saveViewToggle, setSaveViewToggle] = React.useState(false);
    const toggleState = (saveViewToggle) => setSaveViewToggle(!saveViewToggle);
//Header:
/*
<View style={styles.flexedContainer}>
  <Text style={{alignSelf:'center', color:'white'}}>
      LOAD
  </Text>
</View>
*/
    return (
        <View style={{backgroundColor:color_light, width: '100%', height: '100%'}}>


            <View style={{flex:7, alignSelf:'stretch'}}>
                <FlatList
                    data={notesState}
                    renderItem={({item}) => <Item title={item} selected={idState}/>}
                    keyExtractor={item => item.id.toString()}
                />
            </View>

            <View style={{flex:3, alignSelf:'stretch', alignContent:'center', backgroundColor:color_mid}}>
                <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.button.color}
                style={{...styles.flexedButton, width: '90%', margin:2}}
                onPress={() => {dispatch(toggleView(true)); console.log(notesState.map(a => a.name)); navigation.navigate('Write'); }}>
                    <Text style={{...styles.text, color:'white', fontSize:16}}>
                        LOAD NOTE
                    </Text>
                </TouchableHighlight>


                <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.button.color}
                style={{...styles.flexedButton, width: '90%', margin:2}}
                    onPress={() => {console.log(notesState); console.log(idState)}}
                >
                    <Text style={{...styles.text, color:'white',alignSelf:'center', fontSize:16}}>
                        DEBUG LOG
                    </Text>
                </TouchableHighlight>

                <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.button.color}
                style={{...styles.flexedButton, width: '90%', margin:2}}
                    onPress={() => {
                    console.log(idState);
                    if (idState.id != 0){
                        dispatch(deleteNote(idState.id))
                        dispatch(selectId(0));
                    };
                    dispatch(selectId(0));
                    }}
                >
                    <Text style={{...styles.text, color:'white', fontSize:16}}>
                        DELETE
                    </Text>
                </TouchableHighlight>

                <TouchableHighlight
                activeOpacity={0.6}
                underlayColor={styles.button.color}
                style={{...styles.flexedButton, width: '90%', margin:2}}
                    onPress={() => {dispatch(toggleView(true)); console.log(viewState); navigation.goBack()}}
                >
                    <Text style={{...styles.text, color:'white', fontSize:16}}>
                        BACK
                    </Text>
                </TouchableHighlight>
            </View>
        </View>
    );
}

export default LoadView;